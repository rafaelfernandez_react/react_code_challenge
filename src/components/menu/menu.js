import React, { Component } from 'react'
import { Menu, Image } from 'semantic-ui-react'
import Store from '../../store'
import './menu.css'
import home_icon from '../../assets/home/hambar/home_icon.png'
import home_icon_active from '../../assets/home/hambar/red_home_icon.png'
import market_icon from '../../assets/home/hambar/market_icon.png'
import market_icon_active from '../../assets/home/hambar/red_market_icon.png'
import settings_icon from '../../assets/home/hambar/setting_icon.png'
import settings_icon_active from '../../assets/home/hambar/red_setting_icon.png'
import wallet_icon from '../../assets/home/hambar/wallet_icon.png'
import wallet_icon_active from '../../assets/home/hambar/red_wallet_icon.png'

const style = {
    item: {
        color: "#ccc",
        padding: "1.2em 1.5em"
    },
    icon: {
        height:"auto",
        width: "1.8em",
        display: "inline-block",
        marginRight: "1.5em",
    },
};

class HomeMenu extends Component {
    setActiveItem = (e, item) => {
        //Store.launchQueryCancel();
        
        //console.log(Store.current.queryFulfilled);

        //Store.setCurrentItem(item.name);

        //if(item.name === 'Mercado') Store.launchQueryAsync({'area':'body here'});

        //if(item.name === 'Configuración') Store.cleanQuery(null);
    }

    render() {
        return (
            <nav>
                <Menu.Item name='Activos' active={Store.current.currentItem === "Activos"} onClick={this.setActiveItem} style={style.item}>
                    <div className="wrap-item-menu">
                        <Image src={Store.current.currentItem === "Activos" ? home_icon_active : home_icon} style={style.icon}/>
                        <div className="wrap-item-menu__text">Activos</div>
                    </div>
                </Menu.Item>
                <Menu.Item name='Mercado' active={Store.current.currentItem === "Mercado"} onClick={this.setActiveItem} style={style.item}>
                    <div className="wrap-item-menu">
                        <Image src={Store.current.currentItem === "Mercado" ? market_icon_active : market_icon} style={style.icon}/>
                        <div className="wrap-item-menu__text">Mercado</div>
                    </div>
                </Menu.Item>
                <Menu.Item name='Portfolio' active={Store.current.currentItem === "Portfolio"} onClick={this.setActiveItem} style={style.item}>
                    <div className="wrap-item-menu">
                        <Image src={Store.current.currentItem === "Portfolio" ? wallet_icon_active : wallet_icon} style={style.icon}/>
                        <div className="wrap-item-menu__text">Portfolio</div>
                    </div>
                </Menu.Item>
                <Menu.Item name='Configuración' active={Store.current.currentItem === "Configuración"} onClick={this.setActiveItem} style={style.item}>
                    <div className="wrap-item-menu">
                        <Image src={Store.current.currentItem === "Configuración" ? settings_icon_active : settings_icon} style={style.icon}/>
                        <div className="wrap-item-menu__text">Configuración</div>
                    </div>
                </Menu.Item>
            </nav>
        )
    }
}
export default HomeMenu