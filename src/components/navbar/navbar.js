import React, { Component } from 'react'
import { Menu, /*Button,*/ Dropdown } from 'semantic-ui-react'
import Store from '../../store'
import './navbar.css'

class Navbar extends Component {
    constructor(props) {
        super(props);
        this.onClickGetReport = this.onClickGetReport.bind(this);
        
    }

    toggleVisibility = () => {
        Store.setShowSidebar(!Store.current.showSidebar);
    }

    onClickGetReport (e) {
       
    }

    render() {
        return (
            <Menu borderless fixed="top" style={{background: "rgba(32, 35, 46, 1)", zIndex: Store.current.modal.visible ? "0" : "1000", height: "40px"}}>
                <Menu.Item style={{paddingLeft: Store.current.showSidebar ? "260px" : "5px", color:"white", transition: "all .6s ease", paddingTop: "10px", fontWeight: "bold"}} onClick={this.toggleVisibility}>
                    <span id="sidebar_icon" className={Store.current.showSidebar ? "collapse" : ""}><span></span></span>
                    <span style={{fontSize: "1.2em", marginTop: "5px", fontFamily:"GothamBold"}}>{Store.current.currentItem}</span>
                </Menu.Item>

                <Menu.Menu position="right">
                    <Menu.Item>
                        <Dropdown icon='download' className='downloadReport' text='Descargar informe'>
                            <Dropdown.Menu>
                              <Dropdown.Item id="ML" >Valoración por ML</Dropdown.Item>
                              <Dropdown.Item id="ECO" >Valoración por Comparables</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Menu.Item>
                </Menu.Menu>
            </Menu>
        )
    }
}

export default Navbar