import React, { Component } from 'react'
import { Message } from 'semantic-ui-react'
import Store from '../../store'

class CloseableMessage extends Component {
    //state = { visible: true }

  handleDismiss = () => {
      Store.showMessage({header: '', content: '', visible: false})
  }

  render() {
    if (Store.current.message.visible) {
      return (
        <Message
            negative
            icon='user cancel'
            size='mini'
            onDismiss={this.handleDismiss}
            header={Store.current.message.header}
            content={Store.current.message.content}
        />
      )
    }
    else {
        return null
    }
  }
}

export default CloseableMessage
