import React, { Component } from 'react';
import Store from '../../store'
import { DEFAULT_FILTERS } from "../../utils/constants";
import Search from '../searchBox/searchBox';
import Counter from '../counter/counter';
import MoreFilters from '../moreFilters/moreFilters';
import { Responsive, Card, Rail, Image, Label, Segment, Icon, Button, Input} from 'semantic-ui-react';
import './housingData.css'

const style = {
    text_filter: {display: "block", color:"#B7BCC6", fontSize: ".9em"},
    filter_container: {margin: "0 .5em"},
    filter: {position: "absolute", zIndex: "455", background: "#fff", bottom: "-40px", padding: "5px 0", borderRadius: "4px", transition: "all .2s ease-out", height: "2.2em"},
    more_filters: {position: "absolute", zIndex: "450", background: "#fff", bottom: "-22.5em", borderRadius: "5px", transition: "all .2s ease-out", height: "22em", width: "490px", left: "0px", borderTop: "1px solid #d4d4d5"}
};

const WAIT_INTERVAL = 1000;

class HousingData extends Component {
    constructor(props) {
        super(props);
        this.state = {filters:{area: false, bathrooms: false, floor: false, rooms: false, more_filters: false}, areaFocused: false, width: window.innerWidth}
        this.toggleFilter = this.toggleFilter.bind(this);
        // this.search = this.search.bind(this);
        this.handleAreaInput = this.handleAreaInput.bind(this);
        this.toggleAreaFocus = this.toggleAreaFocus.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.getWidth = this.getWidth.bind(this);
        this.counterFilterChanged = this.counterFilterChanged.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ ...this.state, width: window.innerWidth});
    }

    toggleFilter (e) {
        if(typeof e === "object") {
            e.stopPropagation();
            if(this.state.filters[e.target.id]) {
                this.setState({...this.state, filters: {[e.target.id]: false}})
            } else {
                for(let i in this.state) {
                    this.setState({...this.state, filters:{[i]: false}})
                }
                this.setState({...this.state, filters: {[e.target.id]: true}});
            }
        }
        else {
            for(let i in this.state) {
                this.setState({...this.state, filters:{[i]: false}})
            }
        }
    }

    handleAreaInput = (e) => {
       
    }

    _handleKeyPress = (e) => {
       
    }

    counterFilterChanged (property, value) {
       
    }

    getNumberOfActiveFilters () {
        var count = 0;

        for(var filter in Store.current.filters) {
            if(Store.current.filters[filter] !== null)
                count++;
        }

        return (count - DEFAULT_FILTERS);
    }

    toggleAreaFocus () {
        this.setState({...this.state, areaFocused: !this.state.areaFocused});
    }

    componentWillMount() {
        this.timer = null;
    }

    getWidth() {
        let width = "50%";
        if(Store.current.showSidebar)
            width = this.state.width/2 - 130 + "px";

        return width;
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    render() {
        let url = "https://maps.googleapis.com/maps/api/streetview?size=300x300&location=" + Store.current.addressSearch + "&pitch=8&fov=60&key=AIzaSyCyAwtayOFtfq-4nwxIhVQipyWEycjKggE";

        return (
                <Responsive as={Card} className="housingDataAddress" style={{ margin: 0, borderRadius: 0, maxWidth: 800, minWidth: 540, height: 160, width: this.getWidth(), boxShadow: "none", transition: "width .5s ease"}}>
                    <Card.Content style={{padding: 10}}>
                    <div style={{display:"inline-flex", width: "100%"}}>
                        <Image alt='GoogleStreetView' className="googleImage" src={url} style={{verticalAlign: 'top', height:'85px', width:'85px', borderRadius: "2px"}}/>
                        <div className="hdAddress" style={{marginLeft: 10, display:'inline-block', minWidth: 440, width: "100%"}}>
                            <Card.Header style={{fontFamily:"GothamMedium", fontSize: '1em', lineHeight: '1.5em', color: 'black'}}>
                                {Store.current.addressSearch}
                            </Card.Header>
                            <Card.Meta>
                                <span className='date' style={{fontSize: '1em'}}>
                                    {Store.current.fullAddress}
                                </span>
                            </Card.Meta>
                        </div>
                    </div>
                        <Rail style={{width: "65%", marginLeft: "105px", height: "auto", zIndex: 455, padding: 0}} internal position="left">
                            <Search/>
                        </Rail>
                        {/* <Button style={{position: "absolute", right: "5px", top: "56px", height: "32px", width: "72px", padding: "5px", borderRadius: "0 2px 2px 0", background:"#CA1C24", color:"#EFF2F7", fontFamily: "GothamBold", fontSize: ".9em"}} onClick={this.search}>Buscar</Button> */}
                        <div className="filters" style={{position: "relative", marginTop: 10, height: 50, display: "inline-flex", minWidth: 460, width: (Math.floor((this.state.width/2) * 0.65) + 95)}}>
                            <div style={{width: "20%", padding: "0 10px 0 0"}}>
                                <span style={style.text_filter}>Superficie</span>
                                <Input id="area" className="area_input" icon='square' iconPosition='left' style={{width: "100%", height: "2em", padding: "0", paddingLeft: "2em !important"}} value={this.state.areaFocused ? Store.current.filters.area : Store.current.filters.area + ' m\u00b2'} onFocus={this.toggleAreaFocus} onBlur={this.toggleAreaFocus} onChange={this.handleAreaInput} onKeyPress={this._handleKeyPress}/>
                            </div>
                            <div style={{width: "20%", padding: "0 10px 0 0"}}>
                                <span style={style.text_filter}>Baños</span>
                                <Button id="bathrooms" basic style={{width: "100%", height: "2em", margin: 0, padding: 0, paddingLeft: ".5em", position: "relative"}} onClick={this.toggleFilter} icon>
                                    <Icon name="bath" style={{position:"absolute", left:".5em"}}/>
                                    {Store.current.filters.bathrooms}
                                </Button>
                                <div style={{...style.filter, opacity: this.state.filters.bathrooms ? 1 : 0, display: this.state.filters.bathrooms ? "block" : "none", width: "18%"}}>
                                    <Counter id="bathrooms" initialValue={Store.current.filters.bathrooms} handleChange={this.counterFilterChanged} handlerOutside={this.toggleFilter} current={this.state.filters.bathrooms}></Counter>
                                    {/* <Counter root="filters" element="bathrooms" width="100%" current={this.state.filters.bathrooms} handler={this.toggleFilter}></Counter> */}
                                </div>
                            </div>
                            <div style={{width: "20%", padding: "0 10px 0 0"}}>
                                <span style={style.text_filter}>Planta</span>
                                <Button id="floor" basic style={{width: "100%", height: "2em", margin: 0, padding: 0, paddingLeft: ".5em", position: "relative"}} onClick={this.toggleFilter} icon>
                                    <Icon name="building outline" style={{position:"absolute", left:".5em"}}/>
                                    {Store.current.filters.floor}
                                </Button>
                                <div style={{...style.filter, opacity: this.state.filters.floor ? 1 : 0, display: this.state.filters.floor ? "block" : "none", width: "18%"}}>
                                    <Counter id="floor" initialValue={Store.current.filters.floor} handleChange={this.counterFilterChanged} handlerOutside={this.toggleFilter} current={this.state.filters.floor} custom={["SStno", "Plta", "Entplta"]}></Counter>
                                    {/* <Counter width="100%" root="filters" element="floor" current={this.state.filters.floor} handler={this.toggleFilter} customProperties={["SStno", "Plta", "Entplta"]}></Counter> */}
                                </div>
                            </div>
                            <div style={{width: "20%", padding: "0 10px 0 0"}}>
                                <span style={style.text_filter}>Habitaciones</span>
                                <Button id="rooms" basic style={{width: "100%", height: "2em", margin: 0, padding: 0, paddingLeft: ".5em", position: "relative"}} onClick={this.toggleFilter} icon>
                                    <Icon name="bed" style={{position:"absolute", left:".5em"}}/>
                                    {Store.current.filters.rooms}
                                </Button>
                                <div style={{...style.filter, opacity: this.state.filters.rooms ? 1 : 0, display: this.state.filters.rooms ? "block" : "none", width: "18%", paddingRight: 10}}>
                                    <Counter id="rooms" initialValue={Store.current.filters.rooms} handleChange={this.counterFilterChanged} handlerOutside={this.toggleFilter} current={this.state.filters.rooms}></Counter>
                                    {/* <Counter width="5.5em" root="filters" element="rooms" current={this.state.filters.rooms} handler={this.toggleFilter}></Counter> */}
                                </div>
                            </div>
                            <div style={{width: "20%"}}>
                                <span style={{...style.text_filter, width: "max-content"}}>Más características</span>
                                <Segment id="more_filters" style={{width: "100%", height: "2em", margin: 0, padding: 0, cursor: "pointer"}} onClick={this.toggleFilter}><Label circular style={{minWidth: "1em", minHeight: "1em", lineHeight: ".5em", margin: ".35em", backgroundColor: "#CA1C24", color: "#EFF2F7"}}>{this.getNumberOfActiveFilters()}</Label><Icon name="angle down" style={{color:"rgb(106, 108, 113)"}}/></Segment>
                                <div style={{...style.more_filters, opacity: this.state.filters.more_filters ? 1 : 0, display: this.state.filters.more_filters ? "block" : "none", boxShadow: "rgb(183, 188, 198) 1px 0px 10px"}}>
                                    <MoreFilters current={this.state.filters.more_filters} handler={this.toggleFilter}/>
                                </div>
                            </div>
                        </div>
                    </Card.Content>
                </Responsive>
        );
    }

}

export default HousingData;