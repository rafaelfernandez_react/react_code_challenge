import React, { Component } from 'react'
import { Icon } from 'semantic-ui-react'
import Store from '../../store'
import onClickOutside from "react-onclickoutside"

const DEFAULT_VALUE = 1;
const DEFAULT_MIN_VALUE = 1;
const style = {
    icon: {
        border: "1px solid #B7BCC6",
        borderRadius: "50%",
        padding: ".4em",
        width: "1.5rem",
        height: "1.5rem",
        lineHeight: "1rem",
        cursor: "pointer",
        color: "#B7BCC6",
        fontSize: ".5rem",
        margin: 0,
        position: "absolute"
    },
    value: {
        color: "#CA1C24",
        fontWeight: "bold",
        position: "absolute",
        textAlign: "center",
        width: "100%",
        lineHeight: 1.5
    }
}

class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = {value: this.props.initialValue === undefined ? DEFAULT_VALUE : this.props.initialValue, minValue: this.props.minValue === undefined ? DEFAULT_MIN_VALUE : this.props.minValue};
        this.operate = this.operate.bind(this);

    }

    componentWillReceiveProps(nextProps) {
        this.setState({value: this.props.initialValue === undefined ? DEFAULT_VALUE : this.props.initialValue, current: nextProps.current});
    }

    handleClickOutside = evt => {
        evt.stopPropagation();

        if(this.state.current !== undefined)
            if(this.props.handlerOutside !== undefined && typeof this.props.handlerOutside === "function") {
                if(this.props.id !== undefined)
                    this.props.handlerOutside(this.props.id);
                else
                    this.props.handlerOutside();
            }
    }

    operate (e) {
        let isAdding = e.target.id === "add";
        if(this.props.custom === undefined) {
            this.setState({value: isAdding ? this.state.value + 1 : this.state.value - 1}, () => {
                if(this.props.id !== undefined)
                    this.props.handleChange(this.props.id, this.state.value);
                else
                    this.props.handleChange(this.state.value);
            })
        } else {
            const currentIndex = this.props.custom.indexOf(this.state.value);
            let value = this.state.value;
            if(isAdding) {
                if(isNaN(this.state.value) && currentIndex < this.props.custom.length - 1)
                    value = this.props.custom[currentIndex + 1];
                else if(isNaN(this.state.value) && currentIndex === this.props.custom.length - 1)
                    value = 1;
                else if(!isNaN(this.state.value))
                    value++;
            } else {
                if(!isNaN(this.state.value) && this.state.value > 1)
                    value = this.state.value - 1;
                else if(!isNaN(this.state.value) && this.state.value === 1)
                    value = this.props.custom[this.props.custom.length - 1];
                else if(isNaN(this.state.value) && currentIndex > 0)
                    value = this.props.custom[currentIndex - 1];
            }

            this.setState({value: value}, () => {
                this.props.handleChange(this.props.id, this.state.value);
            })
        }
    }

    render() {
        let disabled = false;

        if(this.props.custom === undefined) 
            disabled = this.state.value === this.state.minValue ? true : false;
        else if(isNaN(this.state.value) && this.props.custom.indexOf[this.state.value] === 0)
            disabled = true;

            return (
                <div style={{width: this.props.width + "%", padding: 0, paddingRight: 10}}>
                    <Icon id="substract" name='minus' disabled={disabled} onClick={disabled ? null : this.operate} style={{...style.icon, left: ".5em"}}/>
                    <span style={{...style.value, fontSize: this.state.value.length > 2 ? ".85em" : "1em"}}>{this.state.value}</span>
                    <Icon id="add" name='plus' onClick={this.operate} style={{...style.icon, right: ".5em"}}/>
                </div>
            )
    }
}
export default onClickOutside(Counter)