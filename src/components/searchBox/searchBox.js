import React from 'react'
import PlacesAutocomplete, { geocodeByAddress } from 'react-places-autocomplete'
import Store from '../../store'
import './searchBox.css'

import { COUNTRIES } from "../../utils/constants";
import { Button } from 'semantic-ui-react';

//const options = {
  //componentRestrictions: {country: ['es']}
//}

class Search extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      address: '',
      geocodeResults: null,
      loading: false,
      googleOptions: {
        componentRestrictions: {country: ["es"]}, types: ['address']
      }
    }
    var countries = [];
    this.handleSelect = this.handleSelect.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.renderGeocodeFailure = this.renderGeocodeFailure.bind(this)
    this.renderGeocodeSuccess = this.renderGeocodeSuccess.bind(this)
    this.cleanAddressInput = this.cleanAddressInput.bind(this)
    this.search = this.search.bind(this);
  }

  componentDidMount() {
    
  }

  handleSelect(address) {
    

    geocodeByAddress(address)
      .then((results) => {
        //Store.setLoadingComp({tableHeight: 115, loadingActive: true});
       
        let lat = results[0].geometry.location.lat();
        let lng = results[0].geometry.location.lng();

        console.log(lat);

        this.setState({loading: false});
        
      })
      .catch((error) => {
        console.log(address);
        console.log('Oh no!', error)
        this.setState({
          //geocodeResults: this.renderGeocodeFailure(error),
          loading: false
        })
        Store.setLoc({lat:null, lng: null, cp: null})
      })
  }

  handleChange(address) {
    this.setState({
      address,
      geocodeResults: null
    })
  }

  renderGeocodeFailure(err) {
    return (
      <div className="alert alert-danger" role="alert">
        <strong>Error!</strong> {err}
      </div>
    )
  }

  renderGeocodeSuccess(lat, lng) {
    return (
      <div className="alert alert-success" role="alert">
        <strong>Success!</strong> Geocoder found latitude and longitude: <strong>{lat}, {lng}</strong>
      </div>
    )
  }

  cleanAddressInput() {
    this.setState({address: ''});
  }

  search () { //When the user pushes the search bottom
    
  }

  render() {
    const cssClasses = {
      root: 'form-group',
      input: 'Demo__search-input',
      autocompleteContainer: 'Demo__autocomplete-container',
    }

    const AutocompleteItem = ({ formattedSuggestion }) => (
      <div className="Demo__suggestion-item">
        <i className='fa fa-map-marker Demo__suggestion-icon'/>
        <strong>{formattedSuggestion.mainText}</strong>{' '}
        <small className="text-muted">{formattedSuggestion.secondaryText}</small>
      </div>)

    const inputProps = {
      type: "text",
      value: this.state.address,
      onChange: this.handleChange,
      onBlur: () => { console.log('Blur event!'); },
      onFocus: () => { console.log('Focused!'); },
      autoFocus: true,
      placeholder: "Escribe una dirección",
      name: 'Demo__input',
      id: "my-input-id",
    }

    //const options = {
      //componentRestrictions: {country: "es"}
    //}

    return (
        <div>
          <PlacesAutocomplete
            onSelect={this.handleSelect}
            autocompleteItem={AutocompleteItem}
            onEnterKeyDown={this.handleSelect}
            classNames={cssClasses}
            inputProps={inputProps}
            googleLogo= {false}
            options={this.state.googleOptions}
          />

          {this.state.address.length > 0 ? <span className="close" onClick={this.cleanAddressInput} style={{position: "absolute", top: "62px", right: "5px", fontSize: "1.5rem"}}>&times;</span> : null}
          {this.state.loading ? <div><i className="fa fa-spinner fa-pulse fa-3x fa-fw Demo__spinner" /></div> : null}
          {!this.state.loading && this.state.geocodeResults ?
            <div className='geocoding-results'>{this.state.geocodeResults}</div> :
          null}
          <Button style={{position: "absolute", left: "85%", top: "55px", height: "34px", minWidth: 65, width: "15%", padding: "5px", borderRadius: "0 2px 2px 0", background:"#CA1C24", color:"#EFF2F7", fontFamily: "GothamBold", fontSize: ".9em"}} onClick={this.search}>Buscar</Button>
        </div>
      
    )
  }
}

export default Search