import React, { Component } from 'react'
import { Menu, Image, Confirm } from 'semantic-ui-react'
import user from '../../assets/home/hambar/profile_default.png'
import logout from '../../assets/home/hambar/logout_icon.png'
import Store from '../../store'

const style = {
    item: {
        color: "#ccc",
        background: "rgba(0, 0, 0, 0.28)",
        padding: "10px auto"
    },
    sub_item: {
        color: "#ccc",
        background: "rgba(0, 0, 0, 0.28)",
        padding: "10px 0 10px 4rem"
    }
}

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {showProfileOptions: false, showLogout: false};
        this.onClickUser = this.onClickUser.bind(this);
        this.onClickLogout = this.onClickLogout.bind(this);
        this.confirmLogout = this.confirmLogout.bind(this);
        this.dismissLogout = this.dismissLogout.bind(this);
    }

    onClickUser () {
        console.log("Hey this is your profile");
        this.setState({showProfileOptions: !this.state.showProfileOptions});
    }

    onClickLogout () {
        console.log("You are about to logout, are you sure?")
        Store.setShowSidebar(!Store.current.showSidebar);
        this.setState({showLogout: true});
    }

    confirmLogout () {
        console.log("You logged out");
        Store.setShowSidebar(!Store.current.showSidebar);
        localStorage.removeItem("userJWT");
        Store.setShowLogin(true);
        //this.dismissLogout();
    }

    dismissLogout () {
        Store.setShowSidebar(!Store.current.showSidebar);
        this.setState({showLogout: false})
    }

    render() {
        return (
            <Menu.Menu>
                <Menu.Item style={style.item} onClick={this.onClickUser}>
                    <div style={{ width: "260px", display: "flex", alignItems: "center", transform: Store.current.showSidebar ? "translateX(0)" : "translateX(173px)" }}>
                        <Image src={user} style={{ height: "3em", width: "3.5em", display: "inline-block", marginLeft: "1em", paddingRight: "0.5em" }}/>
                        <div style={{ display: Store.current.showSidebar ? "block" : "none"}}>Company 1</div>
                    </div>
                </Menu.Item>

                <Menu.Menu style={{display: this.state.showProfileOptions ? "block" : "none"}}>
                    <Menu.Item style={style.sub_item} onClick={this.onClickLogout}>
                        <Image src={logout} style={{height: "2em", width: "2.5em", display: "inline-block", paddingRight: "0.5em"}}/>
                        <span>Desconectar</span>
                    </Menu.Item>
                </Menu.Menu>
                 <Confirm
                    open={this.state.showLogout}
                    content="¿Quiere cerrar la sesión?"
                    onCancel={this.dismissLogout}
                    onConfirm={this.confirmLogout}
                    cancelButton="No"
                    confirmButton="Sí"
                    style={{height: "auto"}}/>
            </Menu.Menu>

        )
    }
}
export default Profile