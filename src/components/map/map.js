import React, { Component } from 'react'
import { Map, Marker, Popup, TileLayer, ZoomControl, LayersControl } from 'react-leaflet';
import Control from 'react-leaflet-control';

import L from 'leaflet';

import Store from '../../store'

//https://pastebin.com/CTbe5L8x

const { Overlay } = LayersControl

class LMap extends Component {
    constructor(props) {
        super(props);
        this.state = {         
            height: window.innerHeight, //this is for controlling the map height
            //markers: [{id:-9,lat:0,lng:0}],
            containMarker: false,
            currentIcon:  L.icon({
                iconUrl: require('../../assets/images/poi_current.png'),
                shadowUrl: require('../../assets/images/marker-shadow.png'),
                iconSize:     [50, 58], // size of the icon
                shadowSize:   [44, 44], // size of the shadow
                iconAnchor:   [32, 32], // point of the icon which will correspond to marker's location
                shadowAnchor: [22, 22],  // the same for the shadow
                popupAnchor:  [-5, -35] // point from which the popup should open relative to the iconAnchor
            })
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        // console.log(this);
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);

        this.refs.map.leafletElement.invalidateSize();
    }
    
    componentDidUpdate(prevProps, prevState){
        this.refs.map.leafletElement.invalidateSize();
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    updateWindowDimensions() {
        this.setState({ height: window.innerHeight });
    }

    render() {
        // console.log(this.state.width);
        const position = Store.current.loc;
        return (
            <div className='map-container' style={{/* borderLeft: '2px solid #c2c3c5' */}} >
                <Map ref='map' center={position} zoom={17} style={{ width: '100%', height: this.state.height - 250, top: 45 }} zoomControl={false} > {/*300 because of 50+160+50+40 [header+uDA Value+menu+second header]*/}
                        <ZoomControl position="bottomright" />
                        <TileLayer
                            url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        />
                        <Marker ref='mainAsset' position={position} icon={this.state.currentIcon}></Marker>
                </Map>
            </div>
        )
        
    }
}

export default LMap