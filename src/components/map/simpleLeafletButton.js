import React from 'react';
import Radium from 'radium';

class LeafletButton extends React.Component {
  render() {
    return (
      <button onClick={this.props.onClick}>
        {this.props.children}
      </button>
    );
  }
}

export default Radium(LeafletButton);