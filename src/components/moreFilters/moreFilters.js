import React, { Component } from 'react'
import Store from '../../store'
import { STATUS, CONSTR_TYPE } from "../../utils/constants";
import onClickOutside from "react-onclickoutside"
import { Icon, Grid } from 'semantic-ui-react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

const style = {
    sliderStyle: {width:"50%", padding: "0.5em 0.5em 2em 0em", marginLeft: "15%"},
    handleStyle: {borderColor: "#B7BCC6", background:"rgb(106, 108, 113)", width:".75em", height:".75em", marginTop:"-3px", marginLeft:"-5px", transition:" all 0.2s ease"},
    text_more_filter: {fontWeight: "bold", paddingBottom: ".5em", color: "rgb(106, 108, 113)", fontSize: ".9rem"}
};

const statusMarks = {
    100: "Obra nueva",
    75: "A estrenar",
    50: "Buen estado",
    25: "Reforma parcial",
    0: "Reforma total"
};

const threeOptions = {
    0: <Icon name="delete"/>,
    50: <Icon name="minus"/>,
    100: <Icon name="check"/>
};

class MoreFilters extends Component {
    constructor(props) {
        super(props);
        this.state = {current: false};
        this.getValueForSlider = this.getValueForSlider.bind(this);
        this.setValuesFromSlider = this.setValuesFromSlider.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({current: nextProps.current});
    }

    handleClickOutside = evt => {
        evt.stopPropagation();

        if(this.state.current !== undefined)
            this.props.handler(this.props.element);
    }

    getValueForSlider(value) {
        
    }
    setValuesFromSlider (name, val) {
        
    }

    render() {

        return (
            <div>
                <div style={{marginTop:"1em", marginLeft:"3em", marginBottom: "4em", width: "80%", fontSize:".9rem"}}>
                    <span style={{color:"rgb(106, 108, 113)", fontWeight: "bold"}}>Estado</span>
                    <Slider marks={statusMarks} value={this.getValueForSlider("status")} step={null} included={false} handleStyle={{borderColor: "#B7BCC6", background:"rgb(106, 108, 113)", width:".8em", height:".8em", marginTop:"-3px", marginLeft:"-5px", transition:" all 0.2s ease"}} style={{marginLeft:"1em"}} className="state_filter" onChange={(v) => this.setValuesFromSlider('status', v)}/>
                </div>
                <Grid columns={3} style={{margin: ".5em"}}>
                    <Grid.Row style={{padding: "0"}}>
                        <Grid.Column style={{padding: "0 .5em 1em 1em"}}>
                            <span style={style.text_more_filter}>Aire acondicionado</span>
                            <Slider id="ac" marks={threeOptions} value={this.getValueForSlider("ac")} step={null} included={false} style={style.sliderStyle} handleStyle={style.handleStyle} onChange={(v) => this.setValuesFromSlider('ac', v)}/>
                          </Grid.Column>
                          <Grid.Column style={{padding: "0 .5em 1em 1em"}}>
                            <span style={style.text_more_filter}>Exterior</span>
                            <Slider marks={threeOptions} value={this.getValueForSlider("exterior")} step={null} included={false} style={style.sliderStyle} handleStyle={style.handleStyle}  onChange={(v) => this.setValuesFromSlider('exterior', v)}/>
                        </Grid.Column>
                        <Grid.Column style={{padding: "0 .5em 1em 1em"}}>
                            <span style={style.text_more_filter}>Terraza</span>
                            <Slider marks={threeOptions} value={this.getValueForSlider("terrace")} step={null} included={false} style={style.sliderStyle} handleStyle={style.handleStyle}  onChange={(v) => this.setValuesFromSlider('terrace', v)}/>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row style={{padding: "0"}}>
                        <Grid.Column style={{padding: "0 .5em 1em 1em"}}>
                            <span style={style.text_more_filter}>Agencia</span>
                            <Slider marks={threeOptions} value={this.getValueForSlider("agency")} step={null} included={false} style={style.sliderStyle} handleStyle={style.handleStyle}  onChange={(v) => this.setValuesFromSlider('agency', v)}/>
                        </Grid.Column>
                        <Grid.Column style={{padding: "0 .5em 1em 1em"}}>
                            <span style={style.text_more_filter}>Garaje</span>
                            <Slider marks={threeOptions} value={this.getValueForSlider("garage")} step={null} included={false} style={style.sliderStyle} handleStyle={style.handleStyle}  onChange={(v) => this.setValuesFromSlider('garage', v)}/>
                        </Grid.Column>
                        <Grid.Column style={{padding: "0 .5em 1em 1em"}}>
                            <span style={style.text_more_filter}>Trastero</span>
                            <Slider marks={threeOptions} value={this.getValueForSlider("storage")} step={null} included={false} style={style.sliderStyle} handleStyle={style.handleStyle}  onChange={(v) => this.setValuesFromSlider('storage', v)}/>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row style={{padding: "0"}}>
                        <Grid.Column style={{padding: "0 .5em 1em 1em"}}>
                            <span style={style.text_more_filter}>Ascensor</span>
                            <Slider marks={threeOptions} value={this.getValueForSlider("elevator")} step={null} included={false} style={style.sliderStyle} handleStyle={style.handleStyle}  onChange={(v) => this.setValuesFromSlider('elevator', v)}/>
                        </Grid.Column>
                        <Grid.Column style={{padding: "0 .5em 1em 1em"}}>
                            <span style={style.text_more_filter}>Piscina</span>
                            <Slider marks={threeOptions} value={this.getValueForSlider("pool")} step={null} included={false} style={style.sliderStyle} handleStyle={style.handleStyle}  onChange={(v) => this.setValuesFromSlider('pool', v)}/>
                        </Grid.Column>
                        <Grid.Column style={{padding: "0 .5em 1em 1em"}}>

                      </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }
}

export default onClickOutside(MoreFilters)