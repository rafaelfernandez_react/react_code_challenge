import React from 'react';
import { Responsive, Container, Label, Segment, Icon, Dropdown} from 'semantic-ui-react';
import Counter from '../counter/counter'
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

const threeOptions = {
  0: <Icon name="delete"/>,
  50: <Icon name="minus"/>,
  100: <Icon name="check"/>
};

export default class ComparablesFilters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {visible: false, width: 0, filters: {bathrooms_threshold: 0, rooms_threshold: 0, distance: 50, area_threshold: 100, count: 30, garage: null, outside: true, common_zones: false, new_construction: true}}
    this.toggleFilter = this.toggleFilter.bind(this);
    this.counterFilterChanged = this.counterFilterChanged.bind(this);
    this.getValueForSlider = this.getValueForSlider.bind(this);
    this.setValuesFromSlider = this.setValuesFromSlider.bind(this);
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    this.onRangeSliderChange = this.onRangeSliderChange.bind(this);
    this.getFilterNumber = this.getFilterNumber.bind(this);
  }
  componentDidMount() {
    // console.log(this);
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({...this.state, width: (window.innerWidth / 2) - 20})
  }

  toggleFilter (e) {
    if(typeof e === "object") {
        e.stopPropagation();
        this.setState({...this.state, visible: !this.state.visible})
    }
    else {
        alert('toggleFilter');
    }
  }

  getValueForSlider(value) {
    let val = this.state.filters[value];
      if(val === null)
          return 50;
      else if(val)
          return 100;
      else
          return 0;
  }

  setValuesFromSlider(name, val) {
      let value = true;
      if(val === 0)
          value = false;
      else if(val === 50)
          value = null;

      this.setState({...this.state, filters: {...this.state.filters, [name]: value}});
  }

  counterFilterChanged(value) {
    console.log(value);
  }

  onRangeSliderChange (name, value) {

  }

  getFilterNumber() {
    let total = 0;
    // for(let prop in this.state.filters) {
    //   if(prop === "bathrooms_threshold" || prop === "rooms_threshold")
    //     if(this.state.filters[prop] !== 0)
    //       total++;
    //   else if(this.state.filters[prop] !== null)
    //     total++;
    // }
    // console.log(total);
    return total;
  }

  render() {
    const property_types = [{text: "Piso"}, {text: "Chalet"}];
    return (
      <Container style={{width: "100%"}} >
          <Segment style={styles.input.base} onClick={this.toggleFilter}><Label style={styles.input.label}>Filtros</Label><Label circular style={styles.input.badge}>{this.getFilterNumber()}</Label><Icon name="angle down" style={styles.primaryText}/></Segment>
          <div style={{height: "12em", lineHeight: ".5em", margin: "0px 25px", width: this.state.width - 35, display: this.state.visible ? "block" : "none"}} >
            <div style={{height: "3em", marginBottom: "1em"}}>
              <div style={styles.filter.container}>
                <span style={styles.filter.title}>Habitaciones</span>
                <Counter id="bathrooms" initialValue={0} handleChange={this.counterFilterChanged}></Counter>
              </div>
              <div style={styles.filter.container}>
                <span style={styles.filter.title}>Baños</span>
                <Counter id="bathrooms" initialValue={0} handleChange={this.counterFilterChanged}></Counter>
              </div>
            </div>
            <div style={{height: "3em", marginBottom: "1em"}}>
              <div style={styles.filter.container}>
                <span style={{...styles.filter.title, paddingBottom: 0}}>Superficie</span>
                <Slider defaultValue={50} min={0} max={100} style={{...styles.filter.sliderStyle, width: "75%"}} handleStyle={{...styles.filter.handleStyle, background: "#fff"}} onChange={(v) => this.onRangeSliderChange('area_threshold', v)}/>
              </div>
              <div style={styles.filter.container}>
                <span style={{...styles.filter.title, paddingBottom: 0}}>Distancia</span>
                <Slider defaultValue={20} min={0} max={100} style={{...styles.filter.sliderStyle, width: "75%"}} handleStyle={{...styles.filter.handleStyle, background: "#fff"}} onChange={(v) => this.onRangeSliderChange('area_threshold', v)}/>
              </div>
              <div style={styles.filter.container}>
                <span style={{...styles.filter.title, paddingBottom: 0}}>Número de comparables</span>
                <Slider defaultValue={10} min={0} max={30} style={{...styles.filter.sliderStyle, width: "75%"}} handleStyle={{...styles.filter.handleStyle, background: "#fff"}} onChange={(v) => this.onRangeSliderChange('area_threshold', v)}/>
              </div>
            </div>
            <div style={{height: "3em", marginBottom: "1em"}}>
              <div style={styles.filter.container}>
                <span style={{...styles.filter.title, paddingBottom: 0}}>Garaje</span>
                <Slider marks={threeOptions} value={this.getValueForSlider("garage")} step={null} included={false} style={styles.filter.sliderStyle} handleStyle={styles.filter.handleStyle} onChange={(v) => this.setValuesFromSlider('garage', v)}/>
              </div>
              <div style={styles.filter.container}>
                <span style={{...styles.filter.title, paddingBottom: 0}}>Exterior</span>
                <Slider marks={threeOptions} value={this.getValueForSlider("outside")} step={null} included={false} style={styles.filter.sliderStyle} handleStyle={styles.filter.handleStyle} onChange={(v) => this.setValuesFromSlider('outside', v)}/>
              </div>
              <div style={styles.filter.container}>
                <span style={{...styles.filter.title, paddingBottom: 0}}>Zonas comunes</span>
                <Slider marks={threeOptions} value={this.getValueForSlider("common_zones")} step={null} included={false} style={styles.filter.sliderStyle} handleStyle={styles.filter.handleStyle} onChange={(v) => this.setValuesFromSlider('common_zones', v)}/>
              </div>
              <div style={{...styles.filter.container, marginRight: 0}}>
                <span style={{...styles.filter.title, paddingBottom: 0}}>Obra nueva</span>
                <Slider marks={threeOptions} value={this.getValueForSlider("new_construction")} step={null} included={false} style={styles.filter.sliderStyle} handleStyle={styles.filter.handleStyle} onChange={(v) => this.setValuesFromSlider('new_construction', v)}/>
              </div>
            </div>
          </div>
      </Container>
    );
  }
}
var styles = {
  filtersContainer: {
    // width: "100%",
    // height: "12em",
    // lineHeight: ".5em",
    // margin: "10px 25px !important"
    // paddingTop: 0,
    // paddingLeft: 25,
  },
  primaryText: {
    color: "rgba(0,0,0,.6)"
  },
  secondaryText: {
    color: "#B7BCC6",
  },
  input: {
    base: {
      color: '#fff',
      width: "8em",
      height: "2em",
      marginLeft: "auto",
      marginRight: 10,
      marginTop: 10,
      marginBottom: 0,
      padding: 0,
      cursor: "pointer",
    },
    label: {
      width: "50px",
      minWidth: "1em",
      minHeight: "1em",
      lineHeight: ".5em",
      margin: ".35em",
      color: "rgba(0,0,0,.6)",
      background: "transparent",
      textAlign: "center"
    },
    badge: {
      minWidth: "1em", 
      minHeight: "1em", 
      lineHeight: ".5em", 
      margin: ".35em", 
      backgroundColor: "#CA1C24",
      color: "#eff2f7"
    }
  },
  filter: {
    container: {
      width: "10em",
      position: "relative",
      marginRight: "2em",
      display: "inline-block"
    },
    title: {
      display: "block",
      width: "100%",
      textAlign: "center",
      padding: 5,
      // paddingBottom: 0,
      fontFamily: "GothamLight",
      color: "#6a6c71",
      lineHeight: .75
    },
    sliderStyle: {width:"75%", padding: "0.5em 0.5em 2em 0em", marginLeft: "1em"},
    handleStyle: {borderColor: "#B7BCC6", background:"rgb(106, 108, 113)", width:".75em", height:".75em", marginTop:"-3px", marginLeft:"-5px", transition:" all 0.2s ease"},
  },
};