import React, {Component, cloneElement} from "react";
import {AgGridReact} from "ag-grid-react";
import { CURRENT_SERVER, FLOOR } from "../../utils/constants"
import { Dimmer, Loader, Container, Icon, Label, Segment } from 'semantic-ui-react'
import ComparablesFilters from './comparablesFilters'

import Store from '../../store'

import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/ag-theme-material.css';
import './styles.css';

// const POIS_SELECTED = 30;
const WAIT_INTERVAL = 1000;

const cellStyle = {
    textAlign: 'center'
};

var cb;


class comparablesList2 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            gridOptions: {
                context: {
                    componentParent: this
                }
            },
            columnDefs: this.createColumnDefs(),
            defaultColDef: {
                width: 100
                //headerCheckboxSelection: isFirstColumnHeader
                //headerCheckboxSelectionFilteredOnly: true,         
                //checkboxSelection: isFirstColumn
            },
            rowSelection: "multiple",
            localeText: {
                noRowsToShow: "Ninguna fila que mostrar todavia"
            },
            height: window.innerHeight,
            width: window.innerWidth,
            temporaryAsset: null,
            comesFromMenu: false
        };
        this.onGridReady = this.onGridReady.bind(this);
    }

    counterFilterChanged(e) {
        console.log(e);
    }

    /* componentDidUpdate(prevProps, prevState){
        console.log(prevState);
    } */

    componentDidMount() {
        //this.updateWindowDimensions();
        //window.addEventListener('resize', this.updateWindowDimensions);
        console.log('did mount');
    }
    
    componentWillUnmount() {
        //window.removeEventListener('resize', this.updateWindowDimensions);
    }

    /* updateWindowDimensions() {
        //console.log(this.state.width);
        //this.setState({ ...this.state, height: window.innerHeight, width: window.innerWidth});
        //alert('Controlling the resizing');
    } */

    createColumnDefs() {
        return [ 
            {field: 'id', headerName: 'ID.', hide: true},
            {
                headerName: "",
                field: "rowSel", width: 60, minWidth: 60, maxWidth: 60, cellStyle: cellStyle,
                headerCheckboxSelection: true,
                headerCheckboxSelectionFilteredOnly: true,
                checkboxSelection: true,
                headerCheckboxSelected: true
            },
            {field: 'poi', headerName: 'Poi', width: 50, minWidth: 50, maxWidth: 50, cellRenderer: poiCellRenderer, cellStyle: cellStyle, comparator: sortPoi},
            {field: 'area', headerName: 'Area (m2)', cellStyle: cellStyle},
            {field: 'price', headerName: 'Precio (€/m2)', cellStyle: {...cellStyle, fontWeight: "bold"}},
            {field: 'rooms', headerName: 'Hab.', cellStyle: cellStyle},
            {field: 'bathrooms', headerName: 'Baños', cellStyle: cellStyle},
            {field: 'floor', headerName: 'Planta', cellStyle: cellStyle,  comparator: sortFloor},
            {field: 'outside', headerName: 'Int/Ext', cellStyle: cellStyle},
            {field: 'status', headerName: 'Obra nueva/Segunda mano', cellStyle: cellStyle}
        ]
    }

    methodFromParent(cell) {
        alert(`Parent Component Method from ${cell}!`);
        console.log(cell);
    }

    onGridReady(params) {
        //In order to keep the selection status when change the tab and come back again to Comparables
    }


    render() {
        var message = "Calculando competidores relevantes...";
        return (
            <div style={{height: "100%"}}>
                <div style={{boxSizing: "border-box", width: '100%', height: "100%", overflowX: 'scroll'}} className="ag-theme-material">
                    <ComparablesFilters />

                    <AgGridReact
                        // properties
                        id={Store.current.assetId.id}
                        columnDefs={this.state.columnDefs}
                        defaultColDef={this.state.defaultColDef}
                        rowSelection={this.state.rowSelection}
                        suppressRowClickSelection //Still is multiple selection but a click in a cell doesnt mean a row selection
                        
                        rowData={Store.current.ObservComparablesFulfilled}
                        enableSorting
                        
                        localeText={this.state.localeText}
                        enableColResize={true}
                        gridOptions={this.state.gridOptions}
                    
                        // events
                        onGridReady={this.onGridReady}>

                    </AgGridReact>
                </div>
            </div>
        )
    }
}

function poiCellRenderer(params) {
    var img = document.createElement('img');
    img.style.position = 'relative';
    img.style.top = '7px';

    if (params.value.id) {
        if(params.value.selected) img.src = require('../../assets/images/poi_red_table_' + params.value.id + '.png');
        else img.src = require('../../assets/images/poi_grey_table_' + params.value.id + '.png');

        return img;
    } else {
        return null;
    }
}




function isFirstColumnHeader(params) {
    var displayedColumns = params.columnApi.getAllDisplayedColumns();
    //console.log(displayedColumns[0]);
    var thisIsFirstColumn = displayedColumns[0] === params.column;
    return thisIsFirstColumn;
    
    //if(params.column.colId === 'rowSel') alert(params.api.rowModel.rootNode.selected);

}

function isFirstColumn(params) {
    var displayedColumns = params.columnApi.getAllDisplayedColumns();
    var thisIsFirstColumn = displayedColumns[0] === params.column;
    return thisIsFirstColumn;
}

function sortFloor (valueA, valueB, nodeA, nodeB, isInverted) {
    let firstValue = isNaN(valueA) ? FLOOR[valueA] : valueA;
    let secondValue = isNaN(valueB) ? FLOOR[valueB] : valueB;

    return firstValue - secondValue;
}


function sortPoi (valueA, valueB, nodeA, nodeB, isInverted) {
    return valueA.id - valueB.id;
}

export default comparablesList2;

