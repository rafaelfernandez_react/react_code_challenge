import React, { Component } from 'react'
import Store from '../../store'
import HousingData from '../housingdata/housingData'

import { Container } from 'semantic-ui-react'


class Assets extends Component {
    componentDidMount() {
        
    }

    render() {
        return (
            <Container fluid style={{display: 'inline-flex', backgroundColor: "#fff", boxShadow: "1px 0 10px #B7BCC6" }} >
                <HousingData />
            </Container>
        )
    }
}
export default Assets