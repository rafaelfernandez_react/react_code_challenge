import React, { Component } from 'react'
import ComparablesList2 from '../comparablesList/comparablesList2'
import { Container } from 'semantic-ui-react'

import previewImage from '../../assets/images/preview_socio.png'

import Store from '../../store'

class LeftContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {         
            height: window.innerHeight,
            activeItem: 'COMPARABLES',
            width: window.innerWidth,
            height: window.innerHeight
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        window.addEventListener('resize', this.updateWindowDimensions);
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    updateWindowDimensions() {
        this.setState({ ...this.state, width: window.innerWidth, height: window.innerHeight});
        console.log(window.innerWidth);
    }
    
    render() {
        return (
            <Container style={{ height: '100%', width: '50%', float: 'left', paddingLeft: 10 }} >
                {/* <Container fluid style={{ height: 40, background: 'white', boxShadow: '3px 3px 5px #888888', position: 'relative' }} >
                    <p style={{ textAlign: 'left', position: 'relative', top: 12, fontFamily: 'gothamMedium', fontSize: 12, paddingLeft: 10, color: "#6a6c71"}} >{Store.current.activeAssetItem}</p>
                    <Icon name="chevron right" style={{ color: '#c2c3c5', float: 'right', position: 'relative', bottom: 17, right: '8px' }} />
                    <Icon name="chevron left"  style={{ color: '#c2c3c5', float: 'right', position: 'relative', bottom: 17, right: '5px' }} />
                </Container> */}
                <Container fluid style={{ height: this.state.height - 240, background: 'white'}}>
                    {Store.current.activeAssetItem === "COMPARABLES" ? <ComparablesList2/> : null}
                    {Store.current.activeAssetItem === "FINANCIEROS" ? <Container /> : null}
                    {Store.current.activeAssetItem === "SOCIODEMOGRAFICOS" ?  <Container /> : null}
                </Container>
            </Container>
        )
    }
}

export default LeftContainer