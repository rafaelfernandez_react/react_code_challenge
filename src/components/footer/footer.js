import React from 'react'
import Modal from '../modal/modal'
import Store from '../../store'
import Conditions from '../../conditionsAndPolicy'
import './footer.css'

class Footer extends React.Component {
    constructor() {
        super();
        this.onClickConditions = this.onClickConditions.bind(this);
    }

    onClickConditions(e) {
        e.preventDefault();
        Store.showModal({ title: Conditions.title, content: Conditions.description, visible: true});
    }

    render() {
        return (
            <div style={{display: 'inline-block', position: 'absolute', bottom: '10px', left: 0, right: 0, marginLeft: 'auto', marginRight: 'auto', maxWidth: '760px' }} >
                <div style={{position: 'relative', textAlign: 'center'}} >
                    <a className="bottom-text">  Powered by uDA </a>
                    <a className="bottom-text" href="http://www.urbanDataAnalytics.com" target="_blank" rel="noopener noreferrer">  www.urbanDataAnalytics.com </a>
                    <a className="bottom-text" href="" onClick={this.onClickConditions}>  Condiciones & Políticas de privacidad </a>
                </div>
                <Modal/>
            </div>
        );
    }
}

export default Footer