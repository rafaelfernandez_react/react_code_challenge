import React from 'react'
import './modal.css'
import Store from '../../store'

class Modal extends React.Component {

    renderParagraphs = () => {
        return Store.current.modal.content.map((item, i) => (
            <div key={i} className="content">
                <h3>{item.subtitle}</h3>
                <p>{item.text}</p>
            </div>
        ));
    }

    handleDismiss = () => {
        Store.showModal({title: '', content: '', visible: false});
        Store.setShowSidebar(!Store.current.showSidebar);
    }

    render() {
        if(!Store.current.modal.visible) {
            return null;
        }
        return (
            <div className="overlay">
                <div className="uda_modal">
                    <div style={{marginBottom:"15px"}}>
                        <span className="close" onClick={this.handleDismiss}>&times;</span>
                        <h2 className="title">{Store.current.modal.title}</h2>
                    </div>
                    {this.renderParagraphs()}
                </div>
            </div>
        );
    }
}

export default Modal