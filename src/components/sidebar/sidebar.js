import React, { Component } from 'react'
import { Menu, Image, Container, Divider } from 'semantic-ui-react'
import Store from '../../store'
import background from '../../assets/home/hambar/menu_bg.jpg'
import red_pulse from '../../assets/home/hambar/logo_uda1.png'
import logo_uda from '../../assets/home/hambar/logo_uda2.png'
import HomeMenu from '../menu/menu'
import Modal from '../modal/modal'
import Profile from '../profile/profile'
import Conditions from '../../conditionsAndPolicy'

const style = {
    item: {
        color: "#ccc"
    }
}

class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.onClickConditions = this.onClickConditions.bind(this);
        this.onClickContact = this.onClickContact.bind(this);
    }

    onClickConditions(e) {
        e.preventDefault();
        Store.setShowSidebar(!Store.current.showSidebar);
        Store.showModal({ title: Conditions.title, content: Conditions.description, visible: true});
    }

    onClickContact(event) {
        let formattedSubject = "Solicitando acceso";
        let formattedBody = "Hola \nMe gustar\u00EDa tener acceso a vuestra fantastica plataforma. \n\n ";
        let mailToLink = "mailto:support@urbanDataAnalytics.com?body=" + encodeURIComponent(formattedBody) + "&subject=" + encodeURIComponent(formattedSubject);
        window.location.href = mailToLink;
    }

    render() {
        return (
            <Container>
                <Modal/>
                <Menu vertical fixed="left" style={{position: "fixed", left: Store.current.showSidebar ? "0" : "-260px", backgroundImage: `url(${background})`, height: "100%", backgroundRepeat: "no-repeat", backgroundSize: "cover", width: "260px", border: "none", transition: "all .6s ease", zIndex: 1001}}>
                    <Container style={{marginTop: "15px"}}>
                        <Image src={red_pulse} centered style={{width: "120px"}}/>
                        <Image src={logo_uda} centered style={{marginTop: "15px", width: "120px"}}/>
                    </Container>

                    <HomeMenu/>

                    <Menu.Menu style={{position: "absolute", bottom: "20%", marginBottom: "20px"}}>
                        <Divider section style={{margin: "0 4rem 1rem 1rem", borderBottom: "1px solid rgba(255,255,255,.5)"}}/>
                        <Menu.Item style={style.item} onClick={this.onClickContact}>
                            ¿Necesitas ayuda?
                        </Menu.Item>
                        <Menu.Item style={style.item} onClick={this.onClickConditions}>
                            Condiciones & Políticas de privacidad
                        </Menu.Item>
                    </Menu.Menu>

                    <Profile/>
                </Menu>
            </Container>
        )
    }
}
export default Sidebar