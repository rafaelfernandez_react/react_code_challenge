import React, { Component } from 'react'
import UdaCardTop from '../udaCards/udaCardTop'
import UdaCardBottom from '../udaCards/udaCardBottom'
import { Responsive, Container, Grid } from 'semantic-ui-react'
import './housingValue.css'
import Store from '../../store'

class HousingValue extends Component {
    constructor(props) {
        super(props);
        this.state = {width: window.innerWidth};
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    updateWindowDimensions() {
        this.setState({ ...this.state, width: window.innerWidth});
    }

  render() {
    
    return (
        <Responsive as={Container} className="housingValue" style={{margin: 0, borderRadius: 0, height: 160, backgroundColor: 'white', minWidth: 540, right: 0, width: Store.current.showSidebar ? ((this.state.width/2 - 130) + "px"): "50%", transition: "width .5s ease"}}>
            <Grid columns={2} style={{height: "100%", margin: 0, padding: 10, paddingLeft: 0}}>
            <Grid.Row style={{padding: 0, margin: 0, height: 88}}>
                <Grid.Column style={{padding: 0, minWidth:"245px"}} >
                    <UdaCardTop headerTitle='UDA Value' headerSubtitle=' (Compare)' item="udaValueCompare" bgColor='#FFFFFF' padding='5px 10px 5px 10px' />
                </Grid.Column>
                <Grid.Column style={{padding: 0, paddingLeft: 10, minWidth:"185px", height: "100%"}} >
                    <UdaCardTop headerTitle='UDA Value' headerSubtitle=' (ML)' item="udaValueML" bgColor='#FFFFFF' padding='5px 10px 5px 10px' />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row style={{paddingTop: 0, height: 62 }}>
                <Grid.Column style={{padding: 0, height: "100%", minWidth:"185px"}} >
                    <UdaCardBottom headerTitle='UDA Tipología/Barrio' headerSubtitle='' item="udaNeighborhood" bgColor='#FFFFFF' padding='5px 10px 5px 10px' />
                </Grid.Column>
                <Grid.Column style={{paddingLeft: 9, minWidth:"245px"}} >
                    <UdaCardBottom headerTitle='UDA Ciudad' headerSubtitle='' item="udaCity" bgColor='#FFFFFF' padding='5px 10px 5px 10px' />
                </Grid.Column>
            </Grid.Row>
          </Grid>
        </Responsive>
      )
    }
}

export default HousingValue