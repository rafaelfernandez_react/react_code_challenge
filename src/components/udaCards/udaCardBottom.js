import React, { Component } from 'react'
import { Item, Icon, Container, Dimmer } from 'semantic-ui-react'
import Store from '../../store'
import { getValueWithoutDecimalsIfBiggerThanOneK } from "../../utils/string"
import './udaCards.css'

class UdaCard extends Component {
  render() {
    var cardStyle = {
            backgroundColor: this.props.bgColor,
            padding: this.props.padding,
            height: "100%",
            accuracyBgColor: {
                Alta: "#5bc9b6",
                Media: "#fab94e",
                Baja: "#ca1c24"
            },
            evolution: {
                positive: {
                    icon: "long arrow up",
                    color: "#5bc9b6"
                },
                negative: {
                    icon: "long arrow down",
                    color: "#ca1c24"
                },
                neutral: {
                    icon:"",
                    color:""
                }
            }
    };
 
    return (
        <Container style={{height: "100%"}}>
            
                <Item style={cardStyle} >
                    <Item.Content>
                        <Item.Header className='udaCardHeader' >{this.props.headerTitle}<span>{this.props.headerSubtitle}</span>
                            {/*<span className='precision' style={{backgroundColor: cardStyle.accuracyBgColor[Store.current[this.props.item].accuracy]}} ><span>{Store.current[this.props.item].accuracy}</span> precisión</span>*/}
                            <div className='outBorder' ></div>
                        </Item.Header>
                    <Dimmer.Dimmable as={Item} dimmed={Store.current[this.props.item].loading} blurring style={{height: "100%"}}>
                        <Item.Meta>
                            <Icon name={cardStyle.evolution[Store.current[this.props.item].evolution].icon} style={{top: 3, left: -1, position: 'relative', color: cardStyle.evolution[Store.current[this.props.item].evolution].color}} />
                            <span className='priceUdaBottom'>{getValueWithoutDecimalsIfBiggerThanOneK(Store.current[this.props.item].value)}</span>
                        </Item.Meta>
                        <Item.Description><span className='priceM2Bottom' >{getValueWithoutDecimalsIfBiggerThanOneK(Store.current[this.props.item].value_m) + ' €/m\u00b2'}</span></Item.Description>
                    </Dimmer.Dimmable>
                    </Item.Content>
                </Item>
            {Store.current[this.props.item].loading ? <div className="calculating" style={{position: "absolute", bottom: -15, left: 0, width: "35%", height: "2px", animation: "progress ease-in-out alternate .75s infinite",  animationDelay: this.props.item === "udaNeighborhood" ? ".5s" : "0",  background: "#5bc9b6"}}></div> : null}
        </Container>
      )
    }
}

export default UdaCard