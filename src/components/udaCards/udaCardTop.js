import React, { Component } from 'react'
import Store from '../../store'
import { Item, Icon, Dimmer, Container} from 'semantic-ui-react'
import './udaCards.css'
import { getValueWithoutDecimalsIfBiggerThanOneK } from "../../utils/string"
import Odometer from '../odometer/odometer'
import { getGeojson } from '../../utils/locations';

class UdaCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var cardStyle = {
            backgroundColor: this.props.bgColor,
            padding: this.props.padding,
            height: "100%",
            accuracyBgColor: {
                Alta: "#5bc9b6",
                Media: "#fab94e",
                Baja: "#ca1c24"
            },
            evolution: {
                positive: {
                    icon: "long arrow up",
                    color: "#5bc9b6"
                },
                negative: {
                    icon: "long arrow down",
                    color: "#ca1c24"
                },
                neutral: {
                    icon:"resize vertical",
                    color:"#969696"
                },
                nodata: {
                    icon: "exclamation triangle",
                    color: "#969696"
                }
            }
    };
    var value_m = getValueWithoutDecimalsIfBiggerThanOneK(Store.current[this.props.item].value_m);

    return (
        <Container style={{height: "100%"}}>
            <Item style={cardStyle} >
                <Item.Content>
                    <Item.Header className='udaCardHeader' >{this.props.headerTitle}<span>{this.props.headerSubtitle}</span>
                        {this.props.item === "udaValueML" ? <span className='precision' style={{backgroundColor: cardStyle.accuracyBgColor[Store.current[this.props.item].accuracy], filter: Store.current[this.props.item].loading ? "blur(5px)" : ""}}><span>{Store.current[this.props.item].accuracy}</span> precisión</span> : null}
                        <div className='outBorder' ></div>
                    </Item.Header>
                    <Dimmer.Dimmable as={Item} dimmed={Store.current[this.props.item].loading} blurring style={{height: "100%"}}>
                        <Item.Meta>
                            <Icon name={cardStyle.evolution[Store.current[this.props.item].evolution] ? cardStyle.evolution[Store.current[this.props.item].evolution].icon : cardStyle.evolution['neutral'].icon} style={{top: 4, left: -1, position: 'relative', color: cardStyle.evolution[Store.current[this.props.item].evolution] ? cardStyle.evolution[Store.current[this.props.item].evolution].color : cardStyle.evolution['neutral'].color }} />
                            <span className='priceUdaTop'>{getValueWithoutDecimalsIfBiggerThanOneK(Store.current[this.props.item].value)}</span>
                                <section className='maxminUda' >
                                    <span className='textM' >Máx</span>
                                    <span className='precioM' >{getValueWithoutDecimalsIfBiggerThanOneK(Store.current[this.props.item].value_max)}</span>
                                    <span className='textM' style={{paddingTop: 2}} >Min</span>
                                    <span className='precioM'>{getValueWithoutDecimalsIfBiggerThanOneK(Store.current[this.props.item].value_min)}</span>
                                </section>
                        </Item.Meta>
                    </Dimmer.Dimmable>
                    <Item.Description><span className='priceM2Top'>{value_m === 'Select comparables' ? value_m : value_m + ' €/m\u00b2'}</span></Item.Description>
                    {/*<Item.Description><span className='priceM2Top'>{getValueWithoutDecimalsIfBiggerThanOneK(Store.current[this.props.item].value_m) + ' €/m\u00b2'}</span></Item.Description>*/}
                </Item.Content>
            </Item>
            {Store.current[this.props.item].loading ? <div className="calculating" style={{position: "absolute", bottom: 0, left: 0, width: "35%", height: "2px", animation: "progress ease-in-out alternate .75s infinite", animationDelay: this.props.item === "udaValueCompare" ? "1s" : "0", background: "#5bc9b6"}}></div> : null}
      </Container>
      )
    }
}

export default UdaCard