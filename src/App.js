/* eslint-disable max-len */

import React, { Component } from "react";

import Navbar from './components/navbar/navbar'
import Assets from './components/assets/assets'
import LeftContainer from './components/assets/leftContainer'
import Sidebar from './components/sidebar/sidebar'
import { Container, Menu, Icon, Checkbox, Dimmer, Loader, Button, Modal } from 'semantic-ui-react'
import LMap from './components/map/map'
import Footer from './components/footer/footer'
import background from './assets/login/bg.png' // Tell Webpack this component uses this image
import { connect } from "react-redux";
import Store from "./store";

import 'rc-slider/assets/index.css';

import './App.css'

const style = {
  background: {
    backgroundImage: `url(${background})`,
    height: "100%",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    minHeight: "400px",
    position: "relative",
    width: "100%",
    overflow: "auto"
  }
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      isLoading: true,
      isVenta: true
    };
  }

  toggle = () => {
    
  }

  //handleItemClick = (e, { name }) => this.setState({ activeItem: name });
  handleItemClick = (e, { name }) => Store.setActiveAssetItem(name);

  componentWillMount() {
    console.log('Loading App...');
    
  }

  componentDidMount() {
    this.setState({ isLoading: false });
  }

  render() {
      const { activeAssetItem } = Store.current;
      let twoOptions = {
        0: '',
        1: ''
      };

      return (
        <div className="App-container">
          <Container fluid style={style.background}>
            <Navbar/>
            <Sidebar/>
              <Container className={"page_content " + (Store.current.showSidebar ? "sidebarOn" : "sidebarOff") } style={{paddingLeft: Store.current.showSidebar ? "260px":"0", transition: "all .6s ease", position: "absolute", top: "0", color: "#000", backgroundColor: "#FFF", height: "100%", margin: "0 !important", paddingTop: "40px", overflowY: 'hidden'}}>
                <Assets />
                <Container fluid style={{ height: '100%', background: '#eff2f7', minWidth: "1080px" }} >
                    <Container fluid style={{ height: 50, background: '#eff2f7', paddingLeft: 10, paddingRight: 10, paddingTop: 5 }} >
                      <div style={{ float: 'left', width: '100%', position: "relative", height: "100%" }}>
                        <Menu pointing secondary style={{boxShadow: "rgb(183, 188, 198) 1px 0px 10px"}}>
                          <Menu.Item name='COMPARABLES' active={activeAssetItem === 'COMPARABLES'} onClick={this.handleItemClick} style={{fontWeight: activeAssetItem === 'COMPARABLES' ? "bold": "normal", color:"#6a6c71", marginRight: "1em"}}/>
                          <Menu.Item name='FINANCIEROS' active={activeAssetItem === 'FINANCIEROS'} onClick={this.handleItemClick} style={{fontWeight: activeAssetItem === 'FINANCIEROS' ? "bold": "normal", color:"#6a6c71", marginRight: "1em"}}/>
                          <Menu.Item name='SOCIODEMOGRAFICOS' active={activeAssetItem === 'SOCIODEMOGRAFICOS'} onClick={this.handleItemClick} style={{fontWeight: activeAssetItem === 'SOCIODEMOGRAFICOS' ? "bold": "normal", color:"#6a6c71", marginRight: "1em"}}/>
                        </Menu>
                        <div style={{height: 50, position: 'absolute', bottom: 5, right: 0 }}>
                          <span style={{ position: 'relative', right: 56, top: 12, fontSize: 12, fontFamily: this.state.isVenta ? 'gothamBold' : 'gothamLight', color: this.state.isVenta ? '#6a6c71' : '#717378' }} >VENTA</span>
                            <Checkbox toggle style={{top: 20, left: -40}} className="toggle_rent_sale" onChange={this.toggle}/>
                          <span style={{ position: 'relative', left: -25, top: 12, fontSize: 12, fontFamily: this.state.isVenta ? 'gothamLight' : 'gothamBold', color: this.state.isVenta ? '#717378' : '#6a6c71' }} >ALQUILER</span>
                        </div>
                      </div>
                    </Container>
                    <div>
                      {/* LEFT CONTAINER UNDER TAB AREA */}
                      <LeftContainer />
                      {/* RIGHT CONTAINER UNDER TAB AREA */}
                      <Container style={{ height: '100%', width: '50%', float: 'right', position: 'relative', top: -46, paddingRight: 10, zIndex: 0}} >
                        <Container fluid style={{ height: '100%' }} >
                          <LMap />
                        </Container>
                      </Container>
                    </div>
                </Container>
              </Container>
          </Container>
        </div>
      );
    
  }
}

export default connect(Store => Store)(App);
