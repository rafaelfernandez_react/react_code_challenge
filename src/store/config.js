module.exports = {
    isLogged: '',
    doRefresh: true,
    addressSearch: 'CALLE DE VELAZQUEZ, 53, Madrid, España',
    address: 'dummy address',
    fullAddress: 'Barrio Recoletos, Distrito Salamanca, Madrid',
    loc: {lat: 40.4281797, lng: -3.68423550, cp: 28001},
    showLogin: true,
    message: {header: '', content: '', visible: false},
    modal: {title: '', content: '', visible: false},
    showSidebar: false,
    currentItem: 'Activos',
    user: null,
    queryObservableComparables: false,
    ObservComparablesFulfilled: null,
    ObservComparablesIncComp: false,
    ObservComparablesIncCompFulfilled: null,
    poisAssetComparables: {id:99, lat: 40.4281797, lng: -3.68423550},
    filters: {area: 120, bathrooms: 2, floor: 2, rooms: 3, elevator: null, garage: null, construction_type: 2, agency: null, exterior: null, storage: null, terrace: null, ac: null, pool: null, status: 1, property_type: 4},
    udaNeighborhood: {value:"649564", value_m: "4559", accuracy: "Alta", evolution:"positive", loading: true},
    udaCity: {value:"1087897", value_m: "6185", accuracy: "Media", evolution:"positive", loading: true},
    udaValueML: {value: "528165", value_m: "4401", value_min: "512320", value_max: "544010", accuracy: "Alta", evolution: "positive", loading: true},
    udaValueCompare: {value: "689012", value_m: "5742", value_min: "668342", value_max: "709682", accuracy: "Baja", evolution: "positive", loading: true}, //689012
    //udaValueCompareInitial: null,
    indicatorsData: {evolution: "", segmented: {}, noSegmented: {}},
    assetId: {competitors: null, id: null},
    assetIdObs: null,
    operation: "VENTA",
    activeAssetItem: 'COMPARABLES',
    loadingComparables: {tableHeight: 115, loadingActive: true},
    fRentability: {asset: {value: 1, evolution: 1, chart: [2, 4, 1, 3, 4]}, neighborhood: {value: 1, evolution: 1, chart: [2, 4, 1, 3, 4]}, city: {value: 1, evolution: 1, chart: [2, 4, 1, 3, 4]}},
    fSaleTime: {asset: {value: 1, evolution: 1, chart: [2, 4, 1, 3, 4]}, neighborhood: {value: 1, evolution: 1, chart: [2, 4, 1, 3, 4]}, city: {value: 1, evolution: 1, chart: [2, 4, 1, 3, 4]}},
    fAveragePrice: {asset: {value: 1, evolution: 1, chart: [1, 4, null, 3, 4]}, neighborhood: {value: 1, evolution: 1, chart: [4, 1, 2, 8, 6]}, city: {value: 1, evolution: 1, chart: [3, 8, 5, 4, 7]}},
    fAbsortion: {asset: {value: 1, evolution: 1, chart: [2, 4, 1, 3, 4]}, neighborhood: {value: 1, evolution: 1, chart: [2, 4, 1, 3, 4]}, city: {value: 1, evolution: 1, chart: [2, 4, 1, 3, 4]}},
    nestoriaData: [],
    countNewAssetsCreated: 0,
    initialStatusCompTable: 0,
    selectedTableComparables: null,
    urbanIndicators: {3: {renthog_06_13_M: 0,N_EDA: 0,P_E_TER: 0,P_ED_39: 0,PA_OCIO: 0,N_VIV: 0,P_VIV_VAC: 0,P_VIV_PP: 0,VIV_EDAD_Efc: 0,PA_TP: 0,PA_P: 0,PA_EDU_C: 0,PA_EDU_G: 0}, 6:{renthog_06_13_M: 0,N_EDA: 0,P_E_TER: 0,P_ED_39: 0,PA_OCIO: 0,N_VIV: 0,P_VIV_VAC: 0,P_VIV_PP: 0,VIV_EDAD_Efc: 0,PA_TP: 0,PA_P: 0,PA_EDU_C: 0,PA_EDU_G: 0}}
}