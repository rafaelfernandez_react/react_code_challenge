import { combineEpics, createEpicMiddleware }  from 'redux-observable';
import {createStore, applyMiddleware, compose} from 'redux';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/dom/ajax';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';

import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';


const
    SET_ADDRESS =                           'SET_ADDRESS',
    SET_ADDRESS_SEARCH =                    'SET_ADDRESS_SEARCH',
    SET_LOC =                               'SET_LOC',
    SHOW_LOGIN =                            'SHOW_LOGIN',
    MESSAGE =                               'MESSAGE',
    MODAL =                                 'MODAL',
    SHOW_SIDEBAR =                          'SHOW_SIDEBAR',
    CURRENT_ITEM =                          'CURRENT_ITEM',
    SET_USER =                              'SET_USER',
    FILTERS =                               'FILTERS',
    SET_FULL_ADDRESS =                      'SET_FULL_ADDRESS',
    SET_UDA_CITY =                          'SET_UDA_CITY',
    SET_UDA_NEIGHBORHOOD =                  'SET_UDA_NEIGHBORHOOD',
    QUERY_OBSERV_COMPARABLES =              'QUERY_OBSERV_COMPARABLES',
    OBSERV_COMPARABLES_FULFILLED =          'OBSERV_COMPARABLES_FULFILLED',
    OBSERV_COMPARABLES_INC_COMP =           'OBSERV_COMPARABLES_INC_COMP',
    OBSERV_COMPARABLES_INC_COMP_FULFILLED = 'OBSERV_COMPARABLES_INC_COMP_FULFILLED',
    QUERY_CANCEL =                          'QUERY_CANCEL',
    POIS_ASSET_COMPARABLES =                'POIS_ASSET_COMPARABLES',
    SET_UDA_VALUE_ML =                      'SET_UDA_VALUE_ML',
    SET_UDA_VALUE_COMPARE =                 'SET_UDA_VALUE_COMPARE',
    //SET_UDA_VALUE_COMPARE_INITIAL =         'SET_UDA_VALUE_COMPARE_INITIAL',
    SET_INDICATORS_DATA =                   'SET_INDICATORS_DATA',
    SET_ASSET_ID =                          'SET_ASSET_ID',
    SET_ASSET_ID_OBS =                      'SET_ASSET_ID_OBS',
    SET_OPERATION =                         'SET_OPERATION',
    SET_ACTIVE_ASSET_ITEM =                 'SET_ACTIVE_ASSET_ITEM',
    SET_LOADING_COMPARABLES =               'SET_LOADING_COMPARABLES',
    SET_F_RENTABILITY =                     'SET_F_RENTABILITY',
    SET_F_SALE_TIME =                       'SET_F_SALE_TIME',
    SET_F_AVERAGE_PRICE =                   'SET_F_AVERAGE_PRICE',
    SET_F_ABSORTION =                       'SET_F_ABSORTION',
    SET_NESTORIA_DATA =                     'SET_NESTORIA_DATA',
    NEW_ASSET =                             'NEW_ASSET',
    SET_INITIAL_STATUS_COMP_TABLE =         'SET_INITIAL_STATUS_COMP_TABLE',
    SET_SELECTED_TABLE_COMPARABLE =         'SET_SELECTED_TABLE_COMPARABLE',
    SET_URBAN_INDICATORS =                  'SET_URBAN_INDICATORS'

const Reducers = {
    SET_ADDRESS:                            (address) => ({ address }),
    SET_ADDRESS_SEARCH:                     (addressSearch) => ({ addressSearch }),
    SET_LOC:                                (loc) => ({loc}),
    SHOW_LOGIN:                             (showLogin) => ({showLogin}),
    MESSAGE:                                (message) => ({message}),
    MODAL:                                  (modal) => ({modal}),
    SHOW_SIDEBAR:                           (showSidebar) => ({showSidebar}),
    CURRENT_ITEM:                           (currentItem) => ({currentItem}),
    SET_USER:                               (user) => ({user}),
    FILTERS:                                (filters) => ({filters}),
    SET_FULL_ADDRESS:                       (fullAddress) => ({fullAddress}),
    SET_UDA_CITY:                           (udaCity) => ({udaCity}),
    SET_UDA_NEIGHBORHOOD:                   (udaNeighborhood) => ({udaNeighborhood}),
    QUERY_OBSERV_COMPARABLES:               (queryObservComparables) => ({queryObservComparables}),
    OBSERV_COMPARABLES_FULFILLED:           (ObservComparablesFulfilled) => ({ObservComparablesFulfilled}),
    OBSERV_COMPARABLES_INC_COMP:            (ObservComparablesIncComp) => ({ObservComparablesIncComp}),
    OBSERV_COMPARABLES_INC_COMP_FULFILLED:  (ObservComparablesIncCompFulfilled) => ({ObservComparablesIncCompFulfilled}),
    QUERY_CANCEL:                           () => ({}),
    POIS_ASSET_COMPARABLES:                 (poisAssetComparables) => ({poisAssetComparables}),
    SET_UDA_VALUE_ML:                       (udaValueML) => ({udaValueML}),
    SET_UDA_VALUE_COMPARE:                  (udaValueCompare) => ({udaValueCompare}),
    //SET_UDA_VALUE_COMPARE_INITIAL:          (udaValueCompareInitial) => ({udaValueCompareInitial}),
    SET_INDICATORS_DATA:                    (indicatorsData) => ({indicatorsData}),
    SET_ASSET_ID:                           (assetId) => ({assetId}),
    SET_ASSET_ID_OBS:                       (assetIdObs) => ({assetIdObs}),
    SET_OPERATION:                          (operation) => ({operation}),
    SET_ACTIVE_ASSET_ITEM:                  (activeAssetItem) => ({activeAssetItem}),
    SET_LOADING_COMPARABLES:                (loadingComparables) => ({loadingComparables}),
    SET_F_RENTABILITY:                      (fRentability) => ({fRentability}),
    SET_F_SALE_TIME:                        (fSaleTime) => ({fSaleTime}),
    SET_F_AVERAGE_PRICE:                    (fAveragePrice) => ({fAveragePrice}),
    SET_F_ABSORTION:                        (fAbsortion) => ({fAbsortion}),
    SET_NESTORIA_DATA:                      (nestoriaData) => ({nestoriaData}),
    NEW_ASSET:                              (countNewAssetsCreated) => ({countNewAssetsCreated}),
    SET_INITIAL_STATUS_COMP_TABLE:          (initialStatusCompTable) => ({initialStatusCompTable}),
    SET_SELECTED_TABLE_COMPARABLE:          (selectedTableComparables) => ({selectedTableComparables}),
    SET_URBAN_INDICATORS:                   (urbanIndicators) => ({urbanIndicators})
}

function reducer(currentState, action) {
    if (Reducers[action.type]) {
      return Object.assign({}, currentState,
        Reducers[action.type].call(currentState, ...(action.args)), {action})
    }
    return currentState // DO NOTHING IF NO MATCH
}
  
const store = createStore(reducer, require('./config'),
    compose(
        //applyMiddleware(createEpicMiddleware(getAddress)))
        applyMiddleware(createEpicMiddleware(combineEpics( ))))
    )

function action(x) {
    return (...args) => store.dispatch({type: x, args})
}

export default {
    get current() {
      return store.getState()
    },
    get Store() {
      return store
    },
  
    setAddressFromSearch:           action(SET_ADDRESS_SEARCH),
    setLoc:                         action(SET_LOC),
    setShowLogin:                   action(SHOW_LOGIN),
    showMessage:                    action(MESSAGE),
    showModal:                      action(MODAL),
    setShowSidebar:                 action(SHOW_SIDEBAR),
    setCurrentItem:                 action(CURRENT_ITEM),
    setUser:                        action(SET_USER),
    setFilters:                     action(FILTERS),
    setFullAddress:                 action(SET_FULL_ADDRESS),
    setUdaCity:                     action(SET_UDA_CITY),
    setUdaNeighborhood:             action(SET_UDA_NEIGHBORHOOD),
    launchObservComparables:        action(QUERY_OBSERV_COMPARABLES),
    cleanObservComparables:         action(OBSERV_COMPARABLES_FULFILLED), //We use this to launch an action and then get listening some components
    launchObservComparablesIncComp: action(OBSERV_COMPARABLES_INC_COMP),
    cleanObservComparablesIncComp:  action(OBSERV_COMPARABLES_INC_COMP_FULFILLED),
    launchQueryCancel:              action(QUERY_CANCEL),
    setPOIsInAssetComparables:      action(POIS_ASSET_COMPARABLES),
    setUdaValueML:                  action(SET_UDA_VALUE_ML),
    setUdaValueCompare:             action(SET_UDA_VALUE_COMPARE),
    //setUdaValueCompareInitial:      action(SET_UDA_VALUE_COMPARE_INITIAL),
    setIndicatorsData:              action(SET_INDICATORS_DATA),
    setAssetId:                     action(SET_ASSET_ID),
    setAssetIdObs:                  action(SET_ASSET_ID_OBS),
    setOperation:                   action(SET_OPERATION),
    setActiveAssetItem:             action(SET_ACTIVE_ASSET_ITEM),
    setLoadingComp:                 action(SET_LOADING_COMPARABLES),
    setfRentability:                action(SET_F_RENTABILITY),
    setfSaleTime:                   action(SET_F_SALE_TIME),
    setfAveragePrice:               action(SET_F_AVERAGE_PRICE),
    setfAbsortion:                  action(SET_F_ABSORTION),
    setNestoriaData:                action(SET_NESTORIA_DATA),
    setNewAsset:                    action(NEW_ASSET),
    setInitialStatusCompTable:      action(SET_INITIAL_STATUS_COMP_TABLE),
    setSelectedTableComparable:     action(SET_SELECTED_TABLE_COMPARABLE),
    setUrbanIndicators:             action(SET_URBAN_INDICATORS)
}