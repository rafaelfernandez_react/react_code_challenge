import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import './index.css';
import App from './App';
import 'font-awesome/css/font-awesome.css';
//import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux'; //make the store available to all container components in the application without passing it explicitly. You only need to use it once when you render the root component
import Store from './store';

const rootEl = document.getElementById('root');

ReactDOM.render(
    <Provider store={Store.Store}>
      <App />
    </Provider>,
    rootEl
)
if (module.hot) {
  module.hot.accept('./App', () => {
      const NextApp = require('./App').default
      ReactDOM.render(
          <NextApp />,
          rootEl
      )
  })
}

//registerServiceWorker();
