const EARTH_RADIUS = 6371008.8;

export function getGeojson(lat, lng, admin_levels) {
    const geojson = {
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        lng,
                        lat
                    ]
                },
               "properties": {
                    "admin_levels": admin_levels
                }
            }
        ]
    };

    return JSON.stringify(geojson);
}

export function getDistance(from, to) {
    var dLat = degreesToRadians((from.lat - to.lat));
    var dLon = degreesToRadians((from.lng - to.lng));
    var fromLat = degreesToRadians(from.lat);
    var toLat = degreesToRadians(to.lat);
    
    var a = Math.pow(Math.sin(dLat / 2), 2) +
    Math.pow(Math.sin(dLon / 2), 2) * Math.cos(fromLat) * Math.cos(toLat);
    
    return radiansToLength(2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
}

function degreesToRadians(degrees) {
    if (degrees === null || degrees === undefined)
        console.log('Degrees must have a value');
    else {
        var radians = degrees % 360;
        return radians * Math.PI / 180;
    }
}

function radiansToLength(radians) {
    if (radians === null || radians === undefined)
        console.log('Radians must have a value');
    else {
        return (radians * EARTH_RADIUS).toFixed(2);
    }
}

export function getSimpleAddress(fullAddress) {
    let address = fullAddress.split(",");

    return address[0] + " " + address[1];
}