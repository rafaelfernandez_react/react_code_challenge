// - - - - - - - - - - - - - - - - TEMPORARY CONSTANTS
export const SPAIN_PORTFOLIO = 244;

// ---- THRESHOLD COMPETITORS (FILTERS WILL DEFINE THIS)
export const ROOMS_THRESHOLD = 1;
export const BATHROOMS_THRESHOLD = 1;
export const AREA_THRESHOLD = 0.5;
export const DISTANCE_THRESHOLD = 750;

// - - - - - - - - - - - - - - - - SERVER CONSTANTS
export const PRO = "reds";
export const PRE = "pre";
export const CURRENT_SERVER = PRE;
export const TOKEN_HOURS_LIFE = 12;

// - - - - - - - - - - - - - - - - CONSTANTS THAT MIGHT CHANGE OVER TIME
export const MAX_AREA_ALLOWED = 2000;

// - - - - - - - - - - - - - - - - FILTERS CONSTANTS
export const DEFAULT_FILTERS = 6;
export const FLOOR = {
    "SStno": -0.5, 
    "Plta": 0,
    "Entplta": 0.5
};

export const DEFAULT_NEGOTIATION_VALUE = 0.03;

export const ACCURACY = {
    ALTA:  "Alta",
    MEDIA: "Media",
    BAJA:  "Baja"
};

export const EVOLUTION = {
    POSITIVE: "positive",
    NEGATIVE: "negative",
    NEUTRAL: "neutral"
};

// - - - - - - - - - - - - - - - - ENDPOINTS CONSTANTS
export const OPERATIONS = {
    ALQUILER: 0,
    VENTA: 1
};

export const ADMIN_LEVELS = {
    PAIS: 0,
    COMUNIDAD_AUTONOMA: 1,
    PROVINCIA: 2,
    MUNICIPIO: 3,
    DISTRITO: 4,
    BARRIO: 5,
    SECCION_CENSAL: 6,
    SEGMENTADO: 7 // Creado para el SAAS, no es devuelto por el endpoint
};

export const COUNTRIES = {
    España: "es",
    Italia: "it",
    Peru: "pe"
};

export const BOUNDARIES = {
    PAIS: 10,
    COMUNIDAD_AUTONOMA: 11,
    PROVINCIA: 12,
    MUNICIPIO: 13,
    DISTRITO: 14,
    BARRIO: 15,
    SECCION_CENSAL: 16
};

export const STATUS = {
    BUEN_ESTADO: 1,
    REFORMA_PARCIAL: 2,
    REFORMA_TOTAL: 5,
    A_ESTRENAR: 6
};

export const CONSTR_TYPE = {
    OBRA_NUEVA: 1,
    SEGUNDA_MANO: 2
};

export const REPORT = {
    ML: "ml",
    ECO: "eco"
};

export const PROPERTY_TYPE = {
    ATICO: 1,
    DUPLEX: 2,
    CASA_O_CHALET_INDEP: 3,
    PISO: 4,
    CHALET_PAREADO: 5,
    CHALET_ADOSADO: 6,
    ESTUDIO: 7,
    FINCA_RUSTICA: 8,
    CHALET: 9,
    CASA_DE_PUEBLO: 10,
    CORTIJO: 11,
    CASA_RURAL: 12,
    MASIA: 13,
    CASERIO: 14,
    CASA_TERRERA: 15,
    PALACIO: 16,
    TORRE: 17,
    CASTILLO: 18,
    INDIFERENTE: 19
};

export const INDICATORS = {
    // Urban
    AVERAGE_INCOME: "renthog_06_13_M", // Renta media por hogar disponible (antes de impuestos y cotizaciones por cuenta del trabajador)
    AVERAGE_AGE: "N_EDA", // Edad media población empadronada.
    UNIVERSITY_STUDIES: "P_E_TER", // % de población cesada, con estudios de tercer grado.
    MILLENNIALS: "P_ED_39", // % de población empadronada con edad entre 15 – 39  años.
    CULTURE_SERVICES: "PA_OCIO", // % de proximidad al servicio básico. Cultura y ocio.
    TOTAL_HOUSEHOLDS: "N_VIV", // Número total de viviendas censadas.
    EMPTY_HOUSEHOLDS: "P_VIV_VAC", // % de viviendas vacías censadas.
    PAID_HOUSEHOLDS: "P_VIV_PP", // % de viviendas censadas en propiedad, por compra, totalmente pagada.
    HOUSEHOLD_AGE: "VIV_EDAD_Efc", // Edad correspondiente al edificio sobre la base de su estado actual de conservación y condiciones físicas, debido a obras de mejora y de rehabilitación parcial o integral que se hayan podido producir en la edificación.
    PUBLIC_TRANSPORT_SERVICES: "PA_TP", // % de proximidad al servicio básico. Transporte público.
    PRIVATE_TRANSPORT_SERVICES: "PA_P", // % de proximidad al servicio básico. Transporte privado.
    SCHOOL_SERVICE: "PA_EDU_C", // % de proximidad al servicio básico. Educación. Colegio.
    KINDERGARDEN_SERVICE: "PA_EDU_G", // % de proximidad al servicio básico. Educación. Guardería.
    //Market
    HOUSEHOLD_PRICE: "o_pu", // Outputs precio unitario vivienda
    HOUSEHOLD_AVERAGE_PRICE: "o_pm", // Outputs precio medio vivienda
    INCREMENTAL_QUARTER_PRICE: "o_pu_qq", // Outputs precio unitario incr_trimestral
    NEGOTATION_FACTOR: "s_fn", // Stock Factor de negociacion
    SALE_YIELD: "y_s", // Rentabilidad (yield) Sale
    RENT_YIELD: "y_r", // Rentabilidad (yield) Rent
    TIME_STOCK: "s_t", // Stock tiempo
    OUTPUT_ABSORTION: "o_a", // Output absorción total
    HOUSEHOLDS_UNITS: "o_u", // Número de inmuebles vendidos/alquilados en un trimestre dado.
}