﻿![uDA first Addin 01](https://storage.googleapis.com/uda-rafa-test/assets/logo-filled.png)

## CSS Code challenge

*[Updated on 9th Jul 2019]*

This repository contains a short and minimize copy of the SaaS 2.x project and all of the front-end web files for building a demo and therefore to follow a comprehensive method to be aware and get an idea of this project.

uDA needs to know if you'd fit in this Project so first at all, thanks for your time doing this code challenge

* [Explanation for the Web Designers test](#markdown-header-explanation-for-the-web-designers-test)


----------


This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


In this document:

* [Setting Up Your Development Environment](#markdown-header-setting-up-your-development-environment)
	* [How to get a working copy from the bitbucket repository?](#markdown-header-how-to-get-a-working-copy-from-the-bitbucket-repository)

	* [What editor I should use?](#markdown-header-what-editor-I-should-use)

	* [What files should be ignored for git in my working copy?](#markdown-header-what-files-should-be-ignored-for-git-in-my-working-copy)

	* [How start the localhost development environment?](#markdown-header-how-start-the-localhost-development-environment)

	* [Where the solution is located in the server?](#markdown-header-where-the-solution-is-located-in-the-server)

	* [GIT workflow](#markdown-header-git-workflow)

- - -


# Setting Up Your Development Environment


### How to get a working copy from the bitbucket repository?

 > 1. Take a look at the [Bitbucket repository](https://bitbucket.org/rafaelfernandez_react/front_web_designers_test).
 > 2. Go to a local folder in you computer.
 > 3. Use ```git clone https://rafaelfernandez_react@bitbucket.org/rafaelfernandez_react/front_web_designers_test.git ``` to get the repo. (you can get the https url from the overview page in Bitbucket.
 > 4. Then you will be ready to go.

### What editor I should use?

You can use whatever editor you like but I suggest [Visual Studio Code](https://code.visualstudio.com/). Some advantages of this editor are:

 - Combines the simplicity of a source code editor with powerful developer tooling.
 - Supports macOS, Linux, and Windows.
 - Helps you be instantly productive with syntax highlighting, bracket-matching, auto-indentation, box-selection, snippets, and more.
 - Includes built-in support for IntelliSense code completion, rich semantic code understanding and navigation, and code refactoring.
 -  Includes an interactive debugger, so you can step through source code, inspect variables, view call stacks, and execute commands in the console.
 - Has support for Git so you can work with source control without leaving the editor including viewing pending changes diffs.

### What files should be ignored for git in my working copy?

Right now we have:

> **dependencies**
> /node_modules
> **testing**
> /coverage
> **production**
> /build
> **misc**
> .DS_Store
> .env.local
> .env.development.local
> .env.test.local
> .env.production.local
> .env
> npm-debug.log*
> yarn-debug.log*
> yarn-error.log*
> .vscode/launch.json


### How start the localhost development environment?
You'll be able to launch the localhost server as usual (I suggest ```localhost:8000```


### GIT workflow
Once you do some changes in your working copy, you'll be ready for commit and then push them to the Bitbucket repository.

If you are using Visual Studio Code, you'll see a bullet number with the commit you are ahead.

![vs](https://c1.staticflickr.com/5/4217/34738778743_644f066b3d.jpg "vs")

Therefore:

> 1. Commit your changes.
> 2. Push them to your own branch in the **Front_Web_Designers_test**  Bitbucket repository.

## Explanation for the Web Designers test

Once you get the project working in your localhost, you'll be able to see something like this:

![localhost](https://farm5.staticflickr.com/4763/39143670414_853d5cb435_z.jpg "localhost:8000")

There are different React components. They were named following a logical convention, thus the *sidebar* is the sidebar component, the card you see on the top was wrapped using the *assets* component, the map is *map* component and so.
The project runs in the **App.js** file and from there components are called to render the view above.

We need to show this web app (the Project) in different browsers, devices and screen resolutions. This project fits well for larger screens, but also we'd like to go to a PWA, feeling like a natural app on the device, with an immersive user experience.

We're asking for:

1. According to your criteria, try to render the best design for the sidebar (We are looking for a different dashboard than what we already have). There is a mockup into the `mockup` folder.

2. Once you finish, commit your achievement to a **new bitbucket branch** (this is important!).

Some caveats:
> - We are using Redux in this project, so feel free if you need to add something in our store config (store folder).
>
> - For styling We're using [React Semantics](https://react.semantic-ui.com) and we love inline styling and CSS modules.
>
> - The code challenge should be completed in around 3-4 hours and it must run on IE11 too. Anyway, we know about busy days, so please take the time you need.

If you are stack with the architecture, please just send us an email, we'll try to come back to you as soon as possible.

## Thanks for your collaboration!!

#### The uDA team

:hearts: